from jinja2 import TemplateNotFound
from flask import Blueprint, render_template, abort

main_blueprint = Blueprint("main_bp", __name__, template_folder="templates")


@main_blueprint.route("/")
def main():
    return {"Hello": "World"}


@main_blueprint.route("/show")
def show():
    return render_template("index.html")