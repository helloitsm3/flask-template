from jinja2 import TemplateNotFound
from flask import Blueprint, render_template, abort, request

api_blueprint = Blueprint("api_bp", __name__, template_folder="templates")


@api_blueprint.route("/")
def main():
    return {"Hello": "World, this is an API example"}


@api_blueprint.route("/show", methods=["GET", "POST"])
def show():
    if request.method == "GET":
        return render_template("index.html", text=", This is an API Server!")
    elif request.method == "POST":
        return "Welcome to the POST API server!"