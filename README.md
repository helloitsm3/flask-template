[![Build Status][build-shield]][build-url]
[![Contributors][contributors-shield]][contributors-url]
[![MIT License][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<div>
  <div align="center">
    <h1 style="font-weight: bold">Flask-Heroku Template</h1>
    <a href="https://gitlab.com/helloitsm3/flask-template">
        <img src="./static/assets/logo.png" alt="Logo" width="400" height="auto">
    </a>
    <p align="center" style="margin-top: 30px">
        A template to help you automatically deploy your flask app to heroku using CI/CD
        <br />
        <a href="https://gitlab.com/helloitsm3/flask-template"><strong>Explore the docs »</strong></a>
        <br />
        <a href="https://gitlab.com/helloitsm3/flask-template">View Demo</a>
        ·
        <a href="https://gitlab.com/helloitsm3/flask-template/issues">Report Bug</a>
        ·
        <a href="https://gitlab.com/helloitsm3/flask-template/issues">Request Feature</a>
    </p>
  </div>
</div>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [About the Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [License](#license)
- [Acknowledgements](#acknowledgements)
- [Contributions](#contributions)

<!-- ABOUT THE PROJECT -->

<h2 align="center"> About The Template </h2>

The overall objective of this project is to ensure that the time spent in development is reduced when using templates. Setting up of the environment can be tedious and requires a tremendous amount of time and effort which is why I created this template to ensure the Flask app development is sped up and reduced the effort required.

### Built With

- [Python](https://www.python.org/)
- [Flask](https://flask.palletsprojects.com/en/1.1.x/)
- [Visual Studio Code](https://code.visualstudio.com/)

<!-- GETTING STARTED -->

## Getting Started

This is an example of how you can set up your project locally. To get a local copy up and running follow these simple example steps.

## Prerequisites

This is an example of how to set up your API key to enable automatic deployment to heroku

#### Heroku

```sh
1. Create a Heroku account: https://signup.heroku.com/login
2. Log into Heroku and create a new app
3. Locate to your API key in Profile > Account settings > API Key
4. Once done, copy your App name and API key (You'll need it later)
```

#### Gitlab

```sh
1. Fork this repository
2. Go to the forked repository
3. Navigate to Settings > CI / CD > Variables
4. Expand on Variables and click "Add Variable"
    a. Add "HEROKU_API_KEY" in the "Key" input
    b. Add your Heroku API Key into the "Value" input
    c. Click add variable
5. Go to .gitlab-ci.yml and replace "HEROKU-APP-NAME" with your heroku app name you just created
6. Once that is done, you should be able to see your application deployed to heroku on every commit
```

## Installation

1. Clone the repo

```sh
git clone with HTTPS https://gitlab.com/helloitsm3/flask-template.git
git clone with SSH   git@gitlab.com:helloitsm3/flask-template.git
```

2. Install the necessary libraries

CMD

```sh
# This creates a virtual environment so that when you install the libraries
# it's only isolated to this environment
1. python -m venv venv (optional)
    # This is to activate the virtual environment you just downloaded
    - venv\Scripts\activate

# This installs all the require libraries needed for this project
2. pip install -r requirements.txt

# Sets the main.py file as the main app for flask
3. set FLASK_APP=main.py

# Sets the project environment to development so that the project will refresh upon
# changes to the code without needing to restart the server
4. set FLASK_ENV=development

# Runs the flask project
5. flask run

# Alternatively you can just run this
1. startup_cmd.bat
```

Powershell

```powershell
# This creates a virtual environment so that when you install the libraries
# it's only isolated to this environment
1. python -m venv venv (optional)
    # This is to activate the virtual environment you just downloaded
    - venv\Scripts\activate.ps1

# This installs all the require libraries needed for this project
2. pip install -r requirements.txt

# Sets the main.py file as the main app for flask
3. $env:FLASK_APP="main.py"

# Sets the project environment to development so that the project will refresh upon
# changes to the code without needing to restart the server
4. $env:FLASK_ENV="development"

# Runs the flask project
5. python -m flask run

# Alternatively you can just run this
1. ./startup_ps.ps1
```

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- Acknowledgements -->

## Acknowledgements

- [Img Shields](https://shields.io)
- [Choose an Open Source License](https://choosealicense.com)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[build-shield]: https://img.shields.io/badge/build-passing-brightgreen.svg?style=flat-square
[build-url]: #
[contributors-shield]: https://img.shields.io/badge/contributors-1-orange.svg?style=flat-square
[contributors-url]: https://gitlab.com/helloitsm3/flask-template/-/graphs/master
[license-shield]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license-url]: https://choosealicense.com/licenses/mit
