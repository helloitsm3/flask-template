from flask import Flask
from blueprints.main_bp import main_blueprint
from blueprints.api_bp import api_blueprint

app = Flask(__name__)
app.register_blueprint(main_blueprint, url_prefix="/")
app.register_blueprint(api_blueprint, url_prefix="/api")

if __name__ == "__main__":
    app.run()
